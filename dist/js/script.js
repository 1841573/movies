document.addEventListener("DOMContentLoaded", function () {



    var connexion = new MovieDb();


    console.log(document.location);

    if (document.location.pathname.search("fiche-film.html") > 0) {

        let params = new URL(document.location).searchParams;
        console.log;

        connexion.requeteInfoFilm(params.get("id"));
        connexion.requeteInfoActeurs(params.get("id"))

    } else {

        connexion.requetePopular();
        connexion.requeteNow()


    }


})

class MovieDb {


    constructor() {

        this.APIkey = "81a116cffaa41ebc07b5d8d541aaa113";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "http://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 6;

        this.totalFilmNow = 9;

        this.totalActeur = 6;

    }

    requeteNow() {
        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.requeteRetourNow.bind(this));

        xhr.open("GET", this.baseURL + "movie/now_playing?page=1&language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();
    }

    requeteRetourNow(e) {

        let target = e.currentTarget;

        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            this.afficheNow(data)


        }
    }

    afficheNow(data) {

        for (var i = 0; i < this.totalFilmNow; i++) {



            let unArticle = document.querySelector(".swiper-slide").cloneNode(true);

            unArticle.querySelector("h2").innerText = data[i].title;
            unArticle.querySelector(".vote").innerText = data[i].vote_average;
            unArticle.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);


            let uneImage = unArticle.querySelector("img").setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].poster_path);


            document.querySelector(".swiper-wrapper").appendChild(unArticle);

        }
        var swiper = new Swiper('.swiper-container');
    }

    requetePopular() {

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.requeteRetourPopular.bind(this));

        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();

    }

    requeteRetourPopular(e) {

        let target = e.currentTarget;

        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            this.affichePopular(data)

        }
    }

    affichePopular(data) {

        for (var i = 0; i < this.totalFilm; i++) {
  console.log(data[i].poster_path);

            let unArticle = document.querySelector(".template>article.film").cloneNode(true);

            unArticle.querySelector("h2").innerText = data[i].title;
            unArticle.querySelector(".date").innerText = data[i].release_date;
            unArticle.querySelector(".vote").innerText = data[i].vote_average;


            if (data[i].overview == "") {
                unArticle.querySelector("p.description").innerText = "Description à venir";
            } else {
                unArticle.querySelector("p.description").innerText = data[i].overview;
            }

            let uneImage = unArticle.querySelector("img").setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].poster_path);

            let link = unArticle.querySelector("a");
            link.setAttribute("href", "fiche-film.html?id=" + data[i].id);

            document.querySelector(".liste-films").appendChild(unArticle);
            document.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);
        }

    }

    requeteInfoFilm(idFilm) {

        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.requeteRetourInfoFilm.bind(this));

        xhr.open("GET", this.baseURL + "movie/" + idFilm + "?language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();

    }

    requeteRetourInfoFilm(e) {

        let target = e.currentTarget;

        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            this.afficheInfoFilm(data);
        }
    }

    afficheInfoFilm(data) {

        document.querySelector("h1").innerText = data.title;

        document.querySelector(".date").innerText = data.release_date;
        document.querySelector(".budget").innerText = data.budget + "$";
        document.querySelector(".vote").innerText = data.vote_average;
        document.querySelector(".langue").innerText = data.original_language;

        document.querySelector(".affiche").setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data.poster_path);
        if (data.overview == "") {
            document.querySelector("p.description").innerText = "Description à venir";
        } else {
            document.querySelector("p.description").innerText = data.overview;
        }


    }


    requeteInfoActeurs(idFilm) {


        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.requeteRetourInfoActeurs.bind(this));

        xhr.open("GET", this.baseURL + "movie/" + idFilm + "/credits?api_key="  + this.APIkey);

        xhr.send()


    }

    requeteRetourInfoActeurs(e) {

        let target = e.currentTarget;

        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            this.afficheActeurs(data)


        }
    }


    afficheActeurs(data) {
        console.log(data);


        for (var i = 0; i < this.totalFilm; i++) {

            //console.log(data[i].poster_path);

         var Acteur = document.querySelector(".liste-acteurs>article.acteur").cloneNode(true);

           Acteur.querySelector("h3").innerText = data.cast[i].name;

           document.querySelector(".acteur").appendChild(Acteur);

            // unArticle.querySelector(".vote").innerText = data[i].vote_average ;
            //  unArticle.querySelector("a").setAttribute("href","fiche-film.html?id=" + data[i].id);


            //  let uneImage = unArticle.querySelector("img").setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].poster_path);


        }


    }


}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzY3JpcHQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIkRPTUNvbnRlbnRMb2FkZWRcIiwgZnVuY3Rpb24gKCkge1xyXG5cclxuXHJcblxyXG4gICAgdmFyIGNvbm5leGlvbiA9IG5ldyBNb3ZpZURiKCk7XHJcblxyXG5cclxuICAgIGNvbnNvbGUubG9nKGRvY3VtZW50LmxvY2F0aW9uKTtcclxuXHJcbiAgICBpZiAoZG9jdW1lbnQubG9jYXRpb24ucGF0aG5hbWUuc2VhcmNoKFwiZmljaGUtZmlsbS5odG1sXCIpID4gMCkge1xyXG5cclxuICAgICAgICBsZXQgcGFyYW1zID0gbmV3IFVSTChkb2N1bWVudC5sb2NhdGlvbikuc2VhcmNoUGFyYW1zO1xyXG4gICAgICAgIGNvbnNvbGUubG9nO1xyXG5cclxuICAgICAgICBjb25uZXhpb24ucmVxdWV0ZUluZm9GaWxtKHBhcmFtcy5nZXQoXCJpZFwiKSk7XHJcbiAgICAgICAgY29ubmV4aW9uLnJlcXVldGVJbmZvQWN0ZXVycyhwYXJhbXMuZ2V0KFwiaWRcIikpXHJcblxyXG4gICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgY29ubmV4aW9uLnJlcXVldGVQb3B1bGFyKCk7XHJcbiAgICAgICAgY29ubmV4aW9uLnJlcXVldGVOb3coKVxyXG5cclxuXHJcbiAgICB9XHJcblxyXG5cclxufSlcclxuXHJcbmNsYXNzIE1vdmllRGIge1xyXG5cclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuXHJcbiAgICAgICAgdGhpcy5BUElrZXkgPSBcIjgxYTExNmNmZmFhNDFlYmMwN2I1ZDhkNTQxYWFhMTEzXCI7XHJcblxyXG4gICAgICAgIHRoaXMubGFuZyA9IFwiZnItQ0FcIjtcclxuXHJcbiAgICAgICAgdGhpcy5iYXNlVVJMID0gXCJodHRwczovL2FwaS50aGVtb3ZpZWRiLm9yZy8zL1wiO1xyXG5cclxuICAgICAgICB0aGlzLmltZ1BhdGggPSBcImh0dHA6Ly9pbWFnZS50bWRiLm9yZy90L3AvXCI7XHJcblxyXG4gICAgICAgIHRoaXMubGFyZ2V1ckFmZmljaGUgPSBbXCI5MlwiLCBcIjE1NFwiLCBcIjE4NVwiLCBcIjM0MlwiLCBcIjUwMFwiLCBcIjc4MFwiXTtcclxuXHJcbiAgICAgICAgdGhpcy5sYXJnZXVyVGV0ZUFmZmljaGUgPSBbXCI0NVwiLCBcIjE4NVwiXTtcclxuXHJcbiAgICAgICAgdGhpcy50b3RhbEZpbG0gPSA2O1xyXG5cclxuICAgICAgICB0aGlzLnRvdGFsRmlsbU5vdyA9IDk7XHJcblxyXG4gICAgICAgIHRoaXMudG90YWxBY3RldXIgPSA2O1xyXG5cclxuICAgIH1cclxuXHJcbiAgICByZXF1ZXRlTm93KCkge1xyXG4gICAgICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcclxuXHJcblxyXG4gICAgICAgIHhoci5hZGRFdmVudExpc3RlbmVyKFwicmVhZHlzdGF0ZWNoYW5nZVwiLCB0aGlzLnJlcXVldGVSZXRvdXJOb3cuYmluZCh0aGlzKSk7XHJcblxyXG4gICAgICAgIHhoci5vcGVuKFwiR0VUXCIsIHRoaXMuYmFzZVVSTCArIFwibW92aWUvbm93X3BsYXlpbmc/cGFnZT0xJmxhbmd1YWdlPVwiICsgdGhpcy5sYW5nICsgXCImYXBpX2tleT1cIiArIHRoaXMuQVBJa2V5KTtcclxuXHJcbiAgICAgICAgeGhyLnNlbmQoKTtcclxuICAgIH1cclxuXHJcbiAgICByZXF1ZXRlUmV0b3VyTm93KGUpIHtcclxuXHJcbiAgICAgICAgbGV0IHRhcmdldCA9IGUuY3VycmVudFRhcmdldDtcclxuXHJcbiAgICAgICAgbGV0IGRhdGE7XHJcblxyXG4gICAgICAgIGlmICh0YXJnZXQucmVhZHlTdGF0ZSA9PT0gdGFyZ2V0LkRPTkUpIHtcclxuXHJcbiAgICAgICAgICAgIGRhdGEgPSBKU09OLnBhcnNlKHRhcmdldC5yZXNwb25zZVRleHQpLnJlc3VsdHM7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmFmZmljaGVOb3coZGF0YSlcclxuXHJcblxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBhZmZpY2hlTm93KGRhdGEpIHtcclxuXHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLnRvdGFsRmlsbU5vdzsgaSsrKSB7XHJcblxyXG5cclxuXHJcbiAgICAgICAgICAgIGxldCB1bkFydGljbGUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLnN3aXBlci1zbGlkZVwiKS5jbG9uZU5vZGUodHJ1ZSk7XHJcblxyXG4gICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcImgyXCIpLmlubmVyVGV4dCA9IGRhdGFbaV0udGl0bGU7XHJcbiAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiLnZvdGVcIikuaW5uZXJUZXh0ID0gZGF0YVtpXS52b3RlX2F2ZXJhZ2U7XHJcbiAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiYVwiKS5zZXRBdHRyaWJ1dGUoXCJocmVmXCIsIFwiZmljaGUtZmlsbS5odG1sP2lkPVwiICsgZGF0YVtpXS5pZCk7XHJcblxyXG5cclxuICAgICAgICAgICAgbGV0IHVuZUltYWdlID0gdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJpbWdcIikuc2V0QXR0cmlidXRlKFwic3JjXCIsIHRoaXMuaW1nUGF0aCArIFwid1wiICsgdGhpcy5sYXJnZXVyQWZmaWNoZVszXSArIGRhdGFbaV0ucG9zdGVyX3BhdGgpO1xyXG5cclxuXHJcbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuc3dpcGVyLXdyYXBwZXJcIikuYXBwZW5kQ2hpbGQodW5BcnRpY2xlKTtcclxuXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHZhciBzd2lwZXIgPSBuZXcgU3dpcGVyKCcuc3dpcGVyLWNvbnRhaW5lcicpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlcXVldGVQb3B1bGFyKCkge1xyXG5cclxuICAgICAgICB2YXIgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XHJcblxyXG4gICAgICAgIHhoci5hZGRFdmVudExpc3RlbmVyKFwicmVhZHlzdGF0ZWNoYW5nZVwiLCB0aGlzLnJlcXVldGVSZXRvdXJQb3B1bGFyLmJpbmQodGhpcykpO1xyXG5cclxuICAgICAgICB4aHIub3BlbihcIkdFVFwiLCB0aGlzLmJhc2VVUkwgKyBcIm1vdmllL3BvcHVsYXI/cGFnZT0xJmxhbmd1YWdlPVwiICsgdGhpcy5sYW5nICsgXCImYXBpX2tleT1cIiArIHRoaXMuQVBJa2V5KTtcclxuXHJcbiAgICAgICAgeGhyLnNlbmQoKTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgcmVxdWV0ZVJldG91clBvcHVsYXIoZSkge1xyXG5cclxuICAgICAgICBsZXQgdGFyZ2V0ID0gZS5jdXJyZW50VGFyZ2V0O1xyXG5cclxuICAgICAgICBsZXQgZGF0YTtcclxuXHJcbiAgICAgICAgaWYgKHRhcmdldC5yZWFkeVN0YXRlID09PSB0YXJnZXQuRE9ORSkge1xyXG5cclxuICAgICAgICAgICAgZGF0YSA9IEpTT04ucGFyc2UodGFyZ2V0LnJlc3BvbnNlVGV4dCkucmVzdWx0cztcclxuXHJcbiAgICAgICAgICAgIHRoaXMuYWZmaWNoZVBvcHVsYXIoZGF0YSlcclxuXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGFmZmljaGVQb3B1bGFyKGRhdGEpIHtcclxuXHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLnRvdGFsRmlsbTsgaSsrKSB7XHJcbiAgY29uc29sZS5sb2coZGF0YVtpXS5wb3N0ZXJfcGF0aCk7XHJcblxyXG4gICAgICAgICAgICBsZXQgdW5BcnRpY2xlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi50ZW1wbGF0ZT5hcnRpY2xlLmZpbG1cIikuY2xvbmVOb2RlKHRydWUpO1xyXG5cclxuICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJoMlwiKS5pbm5lclRleHQgPSBkYXRhW2ldLnRpdGxlO1xyXG4gICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcIi5kYXRlXCIpLmlubmVyVGV4dCA9IGRhdGFbaV0ucmVsZWFzZV9kYXRlO1xyXG4gICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcIi52b3RlXCIpLmlubmVyVGV4dCA9IGRhdGFbaV0udm90ZV9hdmVyYWdlO1xyXG5cclxuXHJcbiAgICAgICAgICAgIGlmIChkYXRhW2ldLm92ZXJ2aWV3ID09IFwiXCIpIHtcclxuICAgICAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwicC5kZXNjcmlwdGlvblwiKS5pbm5lclRleHQgPSBcIkRlc2NyaXB0aW9uIMOgIHZlbmlyXCI7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcInAuZGVzY3JpcHRpb25cIikuaW5uZXJUZXh0ID0gZGF0YVtpXS5vdmVydmlldztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgbGV0IHVuZUltYWdlID0gdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJpbWdcIikuc2V0QXR0cmlidXRlKFwic3JjXCIsIHRoaXMuaW1nUGF0aCArIFwid1wiICsgdGhpcy5sYXJnZXVyQWZmaWNoZVszXSArIGRhdGFbaV0ucG9zdGVyX3BhdGgpO1xyXG5cclxuICAgICAgICAgICAgbGV0IGxpbmsgPSB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcImFcIik7XHJcbiAgICAgICAgICAgIGxpbmsuc2V0QXR0cmlidXRlKFwiaHJlZlwiLCBcImZpY2hlLWZpbG0uaHRtbD9pZD1cIiArIGRhdGFbaV0uaWQpO1xyXG5cclxuICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5saXN0ZS1maWxtc1wiKS5hcHBlbmRDaGlsZCh1bkFydGljbGUpO1xyXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiYVwiKS5zZXRBdHRyaWJ1dGUoXCJocmVmXCIsIFwiZmljaGUtZmlsbS5odG1sP2lkPVwiICsgZGF0YVtpXS5pZCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICByZXF1ZXRlSW5mb0ZpbG0oaWRGaWxtKSB7XHJcblxyXG4gICAgICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcclxuXHJcblxyXG4gICAgICAgIHhoci5hZGRFdmVudExpc3RlbmVyKFwicmVhZHlzdGF0ZWNoYW5nZVwiLCB0aGlzLnJlcXVldGVSZXRvdXJJbmZvRmlsbS5iaW5kKHRoaXMpKTtcclxuXHJcbiAgICAgICAgeGhyLm9wZW4oXCJHRVRcIiwgdGhpcy5iYXNlVVJMICsgXCJtb3ZpZS9cIiArIGlkRmlsbSArIFwiP2xhbmd1YWdlPVwiICsgdGhpcy5sYW5nICsgXCImYXBpX2tleT1cIiArIHRoaXMuQVBJa2V5KTtcclxuXHJcbiAgICAgICAgeGhyLnNlbmQoKTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgcmVxdWV0ZVJldG91ckluZm9GaWxtKGUpIHtcclxuXHJcbiAgICAgICAgbGV0IHRhcmdldCA9IGUuY3VycmVudFRhcmdldDtcclxuXHJcbiAgICAgICAgbGV0IGRhdGE7XHJcblxyXG4gICAgICAgIGlmICh0YXJnZXQucmVhZHlTdGF0ZSA9PT0gdGFyZ2V0LkRPTkUpIHtcclxuXHJcbiAgICAgICAgICAgIGRhdGEgPSBKU09OLnBhcnNlKHRhcmdldC5yZXNwb25zZVRleHQpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5hZmZpY2hlSW5mb0ZpbG0oZGF0YSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGFmZmljaGVJbmZvRmlsbShkYXRhKSB7XHJcblxyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJoMVwiKS5pbm5lclRleHQgPSBkYXRhLnRpdGxlO1xyXG5cclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmRhdGVcIikuaW5uZXJUZXh0ID0gZGF0YS5yZWxlYXNlX2RhdGU7XHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5idWRnZXRcIikuaW5uZXJUZXh0ID0gZGF0YS5idWRnZXQgKyBcIiRcIjtcclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLnZvdGVcIikuaW5uZXJUZXh0ID0gZGF0YS52b3RlX2F2ZXJhZ2U7XHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5sYW5ndWVcIikuaW5uZXJUZXh0ID0gZGF0YS5vcmlnaW5hbF9sYW5ndWFnZTtcclxuXHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5hZmZpY2hlXCIpLnNldEF0dHJpYnV0ZShcInNyY1wiLCB0aGlzLmltZ1BhdGggKyBcIndcIiArIHRoaXMubGFyZ2V1ckFmZmljaGVbM10gKyBkYXRhLnBvc3Rlcl9wYXRoKTtcclxuICAgICAgICBpZiAoZGF0YS5vdmVydmlldyA9PSBcIlwiKSB7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJwLmRlc2NyaXB0aW9uXCIpLmlubmVyVGV4dCA9IFwiRGVzY3JpcHRpb24gw6AgdmVuaXJcIjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwicC5kZXNjcmlwdGlvblwiKS5pbm5lclRleHQgPSBkYXRhLm92ZXJ2aWV3O1xyXG4gICAgICAgIH1cclxuXHJcblxyXG4gICAgfVxyXG5cclxuXHJcbiAgICByZXF1ZXRlSW5mb0FjdGV1cnMoaWRGaWxtKSB7XHJcblxyXG5cclxuICAgICAgICB2YXIgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XHJcblxyXG5cclxuICAgICAgICB4aHIuYWRkRXZlbnRMaXN0ZW5lcihcInJlYWR5c3RhdGVjaGFuZ2VcIiwgdGhpcy5yZXF1ZXRlUmV0b3VySW5mb0FjdGV1cnMuYmluZCh0aGlzKSk7XHJcblxyXG4gICAgICAgIHhoci5vcGVuKFwiR0VUXCIsIHRoaXMuYmFzZVVSTCArIFwibW92aWUvXCIgKyBpZEZpbG0gKyBcIi9jcmVkaXRzP2FwaV9rZXk9XCIgICsgdGhpcy5BUElrZXkpO1xyXG5cclxuICAgICAgICB4aHIuc2VuZCgpXHJcblxyXG5cclxuICAgIH1cclxuXHJcbiAgICByZXF1ZXRlUmV0b3VySW5mb0FjdGV1cnMoZSkge1xyXG5cclxuICAgICAgICBsZXQgdGFyZ2V0ID0gZS5jdXJyZW50VGFyZ2V0O1xyXG5cclxuICAgICAgICBsZXQgZGF0YTtcclxuXHJcbiAgICAgICAgaWYgKHRhcmdldC5yZWFkeVN0YXRlID09PSB0YXJnZXQuRE9ORSkge1xyXG5cclxuICAgICAgICAgICAgZGF0YSA9IEpTT04ucGFyc2UodGFyZ2V0LnJlc3BvbnNlVGV4dCkucmVzdWx0cztcclxuXHJcbiAgICAgICAgICAgIHRoaXMuYWZmaWNoZUFjdGV1cnMoZGF0YSlcclxuXHJcblxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcblxyXG4gICAgYWZmaWNoZUFjdGV1cnMoZGF0YSkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xyXG5cclxuXHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLnRvdGFsRmlsbTsgaSsrKSB7XHJcblxyXG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKGRhdGFbaV0ucG9zdGVyX3BhdGgpO1xyXG5cclxuICAgICAgICAgdmFyIEFjdGV1ciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIubGlzdGUtYWN0ZXVycz5hcnRpY2xlLmFjdGV1clwiKS5jbG9uZU5vZGUodHJ1ZSk7XHJcblxyXG4gICAgICAgICAgIEFjdGV1ci5xdWVyeVNlbGVjdG9yKFwiaDNcIikuaW5uZXJUZXh0ID0gZGF0YS5jYXN0W2ldLm5hbWU7XHJcblxyXG4gICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuYWN0ZXVyXCIpLmFwcGVuZENoaWxkKEFjdGV1cik7XHJcblxyXG4gICAgICAgICAgICAvLyB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcIi52b3RlXCIpLmlubmVyVGV4dCA9IGRhdGFbaV0udm90ZV9hdmVyYWdlIDtcclxuICAgICAgICAgICAgLy8gIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiYVwiKS5zZXRBdHRyaWJ1dGUoXCJocmVmXCIsXCJmaWNoZS1maWxtLmh0bWw/aWQ9XCIgKyBkYXRhW2ldLmlkKTtcclxuXHJcblxyXG4gICAgICAgICAgICAvLyAgbGV0IHVuZUltYWdlID0gdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJpbWdcIikuc2V0QXR0cmlidXRlKFwic3JjXCIsIHRoaXMuaW1nUGF0aCArIFwid1wiICsgdGhpcy5sYXJnZXVyQWZmaWNoZVszXSArIGRhdGFbaV0ucG9zdGVyX3BhdGgpO1xyXG5cclxuXHJcbiAgICAgICAgfVxyXG5cclxuXHJcbiAgICB9XHJcblxyXG5cclxufSJdLCJmaWxlIjoic2NyaXB0LmpzIn0=
