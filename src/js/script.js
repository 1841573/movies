document.addEventListener("DOMContentLoaded", function () {



    var connexion = new MovieDb();


    console.log(document.location);

    if (document.location.pathname.search("fiche-film.html") > 0) {

        let params = new URL(document.location).searchParams;
        console.log;

        connexion.requeteInfoFilm(params.get("id"));
        connexion.requeteInfoActeurs(params.get("id"))

    } else {

        connexion.requetePopular();
        connexion.requeteNow()


    }


})

class MovieDb {


    constructor() {

        this.APIkey = "81a116cffaa41ebc07b5d8d541aaa113";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "http://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 6;

        this.totalFilmNow = 9;

        this.totalActeur = 6;

    }

    requeteNow() {
        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.requeteRetourNow.bind(this));

        xhr.open("GET", this.baseURL + "movie/now_playing?page=1&language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();
    }

    requeteRetourNow(e) {

        let target = e.currentTarget;

        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            this.afficheNow(data)


        }
    }

    afficheNow(data) {

        for (var i = 0; i < this.totalFilmNow; i++) {



            let unArticle = document.querySelector(".swiper-slide").cloneNode(true);

            unArticle.querySelector("h2").innerText = data[i].title;
            unArticle.querySelector(".vote").innerText = data[i].vote_average;
            unArticle.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);


            let uneImage = unArticle.querySelector("img").setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].poster_path);


            document.querySelector(".swiper-wrapper").appendChild(unArticle);

        }
        var swiper = new Swiper('.swiper-container');
    }

    requetePopular() {

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.requeteRetourPopular.bind(this));

        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();

    }

    requeteRetourPopular(e) {

        let target = e.currentTarget;

        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            this.affichePopular(data)

        }
    }

    affichePopular(data) {

        for (var i = 0; i < this.totalFilm; i++) {
  console.log(data[i].poster_path);

            let unArticle = document.querySelector(".template>article.film").cloneNode(true);

            unArticle.querySelector("h2").innerText = data[i].title;
            unArticle.querySelector(".date").innerText = data[i].release_date;
            unArticle.querySelector(".vote").innerText = data[i].vote_average;


            if (data[i].overview == "") {
                unArticle.querySelector("p.description").innerText = "Description à venir";
            } else {
                unArticle.querySelector("p.description").innerText = data[i].overview;
            }

            let uneImage = unArticle.querySelector("img").setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].poster_path);

            let link = unArticle.querySelector("a");
            link.setAttribute("href", "fiche-film.html?id=" + data[i].id);

            document.querySelector(".liste-films").appendChild(unArticle);
            document.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);
        }

    }

    requeteInfoFilm(idFilm) {

        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.requeteRetourInfoFilm.bind(this));

        xhr.open("GET", this.baseURL + "movie/" + idFilm + "?language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();

    }

    requeteRetourInfoFilm(e) {

        let target = e.currentTarget;

        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            this.afficheInfoFilm(data);
        }
    }

    afficheInfoFilm(data) {

        document.querySelector("h1").innerText = data.title;

        document.querySelector(".date").innerText = data.release_date;
        document.querySelector(".budget").innerText = data.budget + "$";
        document.querySelector(".vote").innerText = data.vote_average;
        document.querySelector(".langue").innerText = data.original_language;

        document.querySelector(".affiche").setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data.poster_path);
        if (data.overview == "") {
            document.querySelector("p.description").innerText = "Description à venir";
        } else {
            document.querySelector("p.description").innerText = data.overview;
        }


    }


    requeteInfoActeurs(idFilm) {


        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.requeteRetourInfoActeurs.bind(this));

        xhr.open("GET", this.baseURL + "movie/" + idFilm + "/credits?api_key="  + this.APIkey);

        xhr.send()


    }

    requeteRetourInfoActeurs(e) {

        let target = e.currentTarget;

        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            this.afficheActeurs(data)


        }
    }


    afficheActeurs(data) {
        console.log(data);


        for (var i = 0; i < this.totalFilm; i++) {

            //console.log(data[i].poster_path);

         var Acteur = document.querySelector(".liste-acteurs>article.acteur").cloneNode(true);

           Acteur.querySelector("h3").innerText = data.cast[i].name;

           document.querySelector(".acteur").appendChild(Acteur);

            // unArticle.querySelector(".vote").innerText = data[i].vote_average ;
            //  unArticle.querySelector("a").setAttribute("href","fiche-film.html?id=" + data[i].id);


            //  let uneImage = unArticle.querySelector("img").setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].poster_path);


        }


    }


}